#Існує ліст з різними даними, наприклад lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']. Напишіть механізм, який сформує новий list (наприклад lst2), який би містив всі числові змінні, які є в lst1. Майте на увазі, що данні в lst1 не є статичними можуть змінюватись від запуску до запуску.
from copy import deepcopy


list = ['1', 3, True, 'False', 3.33, 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
list2 = deepcopy(list)
list2.remove('False')
for elem in list2:
        if type(elem) in (str, bool):
            list2.remove(elem) 
print('Новий ліст, який містить в собі лише числові змінні з початкового списку', list2)

